﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
namespace BetterToday
{
    public partial class Form4 : MetroFramework.Forms.MetroForm, Interface1
    {
        const int MAX_AVG = 86400;  // avg 최댓값
        public ItemDTO Item { get; set; }
        public ItemRecordDTO ItemRecord { get; set; }
        public int Index { get; set; }
        public Form4()
        {
            InitializeComponent();
            SetUpFont();
        }
        // 부모폼 데이터 리시브
        public void ReceiveData(int index, ItemDTO item, ItemRecordDTO itemRecord)
        {
            Index = index;
            Item = item;
            ItemRecord = itemRecord;
        }

        // 폼 폰트관리
        private void SetUpFont()
        {
            label2.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label3.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            label4.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 14F, FontStyle.Regular);
            confirmBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
            cancelBTN.Font = CustomFont.GetFont(CustomFont.LoadedFont.ESAMANRU, 12F, FontStyle.Bold);
        }

        private int CalculateAvg()
        {
            int itemCount;
            if (ItemRecord.ItemRecordAchievement == Byte.MaxValue)
            {   // 첫 갱신시는 현재값 미포함
                itemCount = Item.ItemCount;
            }
            else
            {
                itemCount = Item.ItemCount - 1;
            }
            if (itemCount < 1 && Item.ItemUpDown == UpDown.UP) return 0;
            else if (itemCount < 1) return MAX_AVG;

            int avg;
            long sum = Item.ItemSum;
            sum -= ItemRecord.ItemRecordValue;
            avg = (int)(sum / itemCount);
            return avg;
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            itemNameTXT.Text = Item.ItemName;
            numInputTXT.Text = ItemRecord.ItemRecordValue.ToString();
            memoTXT.Text = ItemRecord.ItemRecordMemo;
        }

        private void memoTXT_Enter(object sender, EventArgs e)
        {
            if (memoTXT.Text == "메모")
            {
                memoTXT.Text = "";
                memoTXT.ForeColor = SystemColors.WindowText;
            }
        }

        private void memoTXT_Leave(object sender, EventArgs e)
        {
            if (memoTXT.Text.Length == 0)
            {
                memoTXT.Text = "메모";
                memoTXT.ForeColor = SystemColors.ScrollBar;
            }
        }

        private void confirmBTN_Click(object sender, EventArgs e)
        {
            if (numInputTXT.TextLength <= 0)
            {
                MessageBox.Show("횟수를 입력해주세요.");
                return;
            }
            bool isFirst = ItemRecord.ItemRecordAchievement == Byte.MaxValue ? true : false;
            //제약사항 추가 등록 해야함 
            int value = Int32.Parse(numInputTXT.Text);
            int diff = value - ItemRecord.ItemRecordValue;
            string memo = memoTXT.Text;
            if (memo == "메모") memo = null;

            int avg = CalculateAvg();
            if (Item.ItemUpDown == UpDown.UP)
            {   // 이상일 경우
                if (value >= avg) ItemRecord.ItemRecordAchievement = 2;
                else if(value==0) ItemRecord.ItemRecordAchievement = 0;
                else ItemRecord.ItemRecordAchievement = 1;
            }
            else
            {   // 이하일 경우
                if (value <= avg) ItemRecord.ItemRecordAchievement = 2;
                else if (value == 0) ItemRecord.ItemRecordAchievement = 0;
                else ItemRecord.ItemRecordAchievement = 1;
            }

            ItemRecord.ItemRecordMemo = memo;
            ItemRecord.ItemRecordValue = value;

            DAO dao = new DAO();
            ReturnCode res = dao.UpdateItemRecord(ItemRecord);
            dao.UpdateItemSum(Item.ItemId, diff);
            if (res == ReturnCode.SUCCESS)
            {
                if (isFirst)
                {
                    dao.UpdateItemCounts(new List<int> { Item.ItemId },1);
                }
                if (this.Owner.Owner is Form1) {
                    Form1 owner = this.Owner.Owner as Form1;
                    owner.LoadItemList();
                    owner.LoadItemRecords(Index, Index + 1);
                    owner.UpdatePanelItemList();
                    owner.UpdateItemRecords(Index, Index + 1);
                }
                this.Close();
            }
            else MessageBox.Show("입력이 실패하였습니다.");            

        }
        private void cancelBTN_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
  
}
