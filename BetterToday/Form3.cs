﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BetterToday
{
    public partial class Form3 : MetroFramework.Forms.MetroForm
    {
        private DAO dao = new DAO();
        private string id;
        private string pw;
        public Form3()
        {
            InitializeComponent();
        }
        

        public string passId
        {
            get { return this.id; }
        }
        public string passPw
        {
            get { return this.pw; }
        }
        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        
            if (this.textBox1.Text == "")
            {
                MessageBox.Show("로그인 아이디를 입력하세요", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.textBox1.Focus();
            }
            else if (this.textBox2.Text == "")
            {
                MessageBox.Show("로그인 비밀번호를 입력하세요", "알림", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.textBox2.Focus();
            }
            else
            {
                ReturnCode result = dao.SignUp(textBox1.Text, textBox2.Text);
                switch (result)
                {
                    case ReturnCode.SUCCESS:
                        if (MessageBox.Show("회원가입 성공") == DialogResult.OK)
                        {
                            id = textBox1.Text;
                            pw = textBox2.Text;
                            this.Close();
                        }
                        break;
                    case ReturnCode.DUPLICATE:
                        MessageBox.Show("중복된 아이디가 존재합니다.");
                        break;
                    case ReturnCode.FAIL:
                        MessageBox.Show("회원가입 실패");
                        break;
                }

            }
        }
   
    }
}
