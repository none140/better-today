﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Text;
using System.Drawing;

namespace BetterToday
{
    static class CustomFont
    {
        public enum LoadedFont
        {
            SAMLIP_HOPANG_BASIC,
            TAIPO_SSANGMOONDONG,
            ESAMANRU,
            NEXONLV1GOTHIC
        }
        public static readonly String[] FONTS =
        {// 폰트 스트링 상수
            "Sandoll 삼립호빵체 TTF Basic",
            "타이포_쌍문동 B",
            "이사만루체 Light",
            "넥슨Lv1고딕"
        };
        private static PrivateFontCollection privateFonts;

        static CustomFont()
        {
            privateFonts = new PrivateFontCollection();
            privateFonts.AddFontFile("./Resources/SDSamliphopangcheTTFBasic.ttf");
            privateFonts.AddFontFile("./Resources/Typo_SsangmunDongB.ttf");
            privateFonts.AddFontFile("./Resources/esamanru Light.ttf");
            privateFonts.AddFontFile("./Resources/NEXONLv1GothicRegular.ttf");
        }
        public static Font GetFont(LoadedFont font, float size, FontStyle fontStyle = FontStyle.Regular)
        {
            return new Font(privateFonts.Families.Where(x => x.Name ==FONTS[(int)font]).FirstOrDefault(), size, fontStyle);
        }

    }
}
