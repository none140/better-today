﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterToday
{
    public enum ItemType
    {
        COUNT = 1,
        TIME, LEAD_TIME
    }

    public enum ReturnCode
    {
        SUCCESS = 1,
        FAIL = -1,
        DUPLICATE = 0
    }

    public enum UpDown
    {
        DOWN=0,
        UP
    }
}
