﻿namespace BetterToday
{
    partial class Form8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.confirmBTN = new System.Windows.Forms.Button();
            this.downRadio = new System.Windows.Forms.RadioButton();
            this.upRadio = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.itemNameTXT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.startTimePicker = new MyDateTimePicker();
            this.endDateTimePicker = new MyDateTimePicker();
            this.itemTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // confirmBTN
            // 
            this.confirmBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))));
            this.confirmBTN.FlatAppearance.BorderSize = 0;
            this.confirmBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmBTN.Font = new System.Drawing.Font("타이포_쌍문동 B", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmBTN.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.confirmBTN.Location = new System.Drawing.Point(14, 577);
            this.confirmBTN.Name = "confirmBTN";
            this.confirmBTN.Size = new System.Drawing.Size(357, 53);
            this.confirmBTN.TabIndex = 72;
            this.confirmBTN.Text = "확인";
            this.confirmBTN.UseVisualStyleBackColor = false;
            this.confirmBTN.Click += new System.EventHandler(this.confirmBTN_Click);
            // 
            // downRadio
            // 
            this.downRadio.AutoSize = true;
            this.downRadio.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downRadio.Location = new System.Drawing.Point(203, 521);
            this.downRadio.Name = "downRadio";
            this.downRadio.Size = new System.Drawing.Size(88, 27);
            this.downRadio.TabIndex = 69;
            this.downRadio.Text = "DOWN";
            this.downRadio.UseVisualStyleBackColor = true;
            // 
            // upRadio
            // 
            this.upRadio.AutoSize = true;
            this.upRadio.Checked = true;
            this.upRadio.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upRadio.Location = new System.Drawing.Point(120, 521);
            this.upRadio.Name = "upRadio";
            this.upRadio.Size = new System.Drawing.Size(56, 27);
            this.upRadio.TabIndex = 68;
            this.upRadio.TabStop = true;
            this.upRadio.Text = "UP";
            this.upRadio.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.itemNameTXT);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(14, 172);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(357, 31);
            this.panel1.TabIndex = 62;
            // 
            // itemNameTXT
            // 
            this.itemNameTXT.BackColor = System.Drawing.SystemColors.Window;
            this.itemNameTXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.itemNameTXT.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameTXT.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.itemNameTXT.Location = new System.Drawing.Point(7, 5);
            this.itemNameTXT.Name = "itemNameTXT";
            this.itemNameTXT.Size = new System.Drawing.Size(329, 20);
            this.itemNameTXT.TabIndex = 19;
            this.itemNameTXT.Text = "Item name";
            this.itemNameTXT.Enter += new System.EventHandler(this.textBox1_Enter_1);
            this.itemNameTXT.Leave += new System.EventHandler(this.textBox1_Leave_1);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 33);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(246, 49);
            this.label1.TabIndex = 61;
            this.label1.Text = "Item Setting";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // startTimePicker
            // 
            this.startTimePicker.CalendarFont = new System.Drawing.Font("타이포_쌍문동 B", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTimePicker.CalendarTitleBackColor = System.Drawing.Color.Gray;
            this.startTimePicker.CalendarTitleForeColor = System.Drawing.Color.White;
            this.startTimePicker.CustomFormat = " 시작 : yyyy년 MM월 dd일";
            this.startTimePicker.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startTimePicker.Location = new System.Drawing.Point(14, 269);
            this.startTimePicker.Name = "startTimePicker";
            this.startTimePicker.Size = new System.Drawing.Size(357, 31);
            this.startTimePicker.TabIndex = 93;
            this.startTimePicker.MouseDown += new System.Windows.Forms.MouseEventHandler(this.startTimePicker_MouseDown);
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.CalendarFont = new System.Drawing.Font("타이포_쌍문동 B", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.endDateTimePicker.CalendarTitleBackColor = System.Drawing.Color.Gray;
            this.endDateTimePicker.CalendarTitleForeColor = System.Drawing.Color.White;
            this.endDateTimePicker.CustomFormat = " 종료 : yyyy년 MM월 dd일";
            this.endDateTimePicker.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDateTimePicker.Location = new System.Drawing.Point(14, 366);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(357, 31);
            this.endDateTimePicker.TabIndex = 96;
            this.endDateTimePicker.Value = new System.DateTime(3000, 1, 1, 20, 23, 0, 0);
            this.endDateTimePicker.MouseDown += new System.Windows.Forms.MouseEventHandler(this.endDateTimePicker_MouseDown);
            // 
            // itemTypeComboBox
            // 
            this.itemTypeComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.itemTypeComboBox.DisplayMember = "항목을 선택하시오";
            this.itemTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.itemTypeComboBox.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemTypeComboBox.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.itemTypeComboBox.FormattingEnabled = true;
            this.itemTypeComboBox.Items.AddRange(new object[] {
            "1. 횟수",
            "2. 정해진 시간",
            "3. 시간 간격"});
            this.itemTypeComboBox.Location = new System.Drawing.Point(14, 457);
            this.itemTypeComboBox.Name = "itemTypeComboBox";
            this.itemTypeComboBox.Size = new System.Drawing.Size(357, 31);
            this.itemTypeComboBox.TabIndex = 97;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(10, 421);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 23);
            this.label2.TabIndex = 98;
            this.label2.Text = "타입";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(10, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 23);
            this.label3.TabIndex = 99;
            this.label3.Text = "아이템 이름";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(10, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 23);
            this.label4.TabIndex = 100;
            this.label4.Text = "시작 날짜";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("타이포_쌍문동 B", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(10, 328);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 23);
            this.label5.TabIndex = 101;
            this.label5.Text = "종료 날짜";
            // 
            // Form8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 647);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.itemTypeComboBox);
            this.Controls.Add(this.endDateTimePicker);
            this.Controls.Add(this.startTimePicker);
            this.Controls.Add(this.confirmBTN);
            this.Controls.Add(this.downRadio);
            this.Controls.Add(this.upRadio);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("나눔고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.MaximizeBox = false;
            this.Name = "Form8";
            this.Padding = new System.Windows.Forms.Padding(22, 68, 22, 23);
            this.Resizable = false;
            this.Load += new System.EventHandler(this.Form8_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button confirmBTN;
        private System.Windows.Forms.RadioButton downRadio;
        private System.Windows.Forms.RadioButton upRadio;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox itemNameTXT;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox itemTypeComboBox;
        private System.Windows.Forms.Label label2;
        private MyDateTimePicker startTimePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private MyDateTimePicker endDateTimePicker;
    }
}