﻿namespace BetterToday
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.confirmBTN = new System.Windows.Forms.Button();
            this.cancelBTN = new System.Windows.Forms.Button();
            this.itemNameTXT = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.numInputTXT = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.memoTXT = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(67, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 49);
            this.label1.TabIndex = 0;
            this.label1.Text = "Item Detail";
            // 
            // confirmBTN
            // 
            this.confirmBTN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(115)))), ((int)(((byte)(217)))));
            this.confirmBTN.FlatAppearance.BorderSize = 0;
            this.confirmBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmBTN.Font = new System.Drawing.Font("이사만루체 Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmBTN.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.confirmBTN.Location = new System.Drawing.Point(67, 496);
            this.confirmBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.confirmBTN.Name = "confirmBTN";
            this.confirmBTN.Size = new System.Drawing.Size(105, 45);
            this.confirmBTN.TabIndex = 34;
            this.confirmBTN.Text = "확인";
            this.confirmBTN.UseVisualStyleBackColor = false;
            this.confirmBTN.Click += new System.EventHandler(this.confirmBTN_Click);
            // 
            // cancelBTN
            // 
            this.cancelBTN.BackColor = System.Drawing.Color.Gainsboro;
            this.cancelBTN.FlatAppearance.BorderSize = 0;
            this.cancelBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelBTN.Font = new System.Drawing.Font("이사만루체 Bold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBTN.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.cancelBTN.Location = new System.Drawing.Point(192, 496);
            this.cancelBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelBTN.Name = "cancelBTN";
            this.cancelBTN.Size = new System.Drawing.Size(105, 45);
            this.cancelBTN.TabIndex = 35;
            this.cancelBTN.Text = "취소";
            this.cancelBTN.UseVisualStyleBackColor = false;
            this.cancelBTN.Click += new System.EventHandler(this.cancelBTN_Click_1);
            // 
            // itemNameTXT
            // 
            this.itemNameTXT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.itemNameTXT.BackColor = System.Drawing.SystemColors.Window;
            this.itemNameTXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.itemNameTXT.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemNameTXT.ForeColor = System.Drawing.SystemColors.WindowText;
            this.itemNameTXT.Location = new System.Drawing.Point(51, 205);
            this.itemNameTXT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.itemNameTXT.Name = "itemNameTXT";
            this.itemNameTXT.ReadOnly = true;
            this.itemNameTXT.Size = new System.Drawing.Size(262, 23);
            this.itemNameTXT.TabIndex = 36;
            this.itemNameTXT.Text = "Item name";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel4.Location = new System.Drawing.Point(51, 232);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(262, 1);
            this.panel4.TabIndex = 37;
            // 
            // numInputTXT
            // 
            this.numInputTXT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numInputTXT.BackColor = System.Drawing.SystemColors.Window;
            this.numInputTXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numInputTXT.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numInputTXT.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.numInputTXT.Location = new System.Drawing.Point(51, 309);
            this.numInputTXT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numInputTXT.Name = "numInputTXT";
            this.numInputTXT.Size = new System.Drawing.Size(262, 23);
            this.numInputTXT.TabIndex = 38;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Location = new System.Drawing.Point(51, 337);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 1);
            this.panel1.TabIndex = 39;
            // 
            // memoTXT
            // 
            this.memoTXT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoTXT.BackColor = System.Drawing.SystemColors.Window;
            this.memoTXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.memoTXT.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoTXT.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.memoTXT.Location = new System.Drawing.Point(51, 420);
            this.memoTXT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.memoTXT.Name = "memoTXT";
            this.memoTXT.Size = new System.Drawing.Size(262, 23);
            this.memoTXT.TabIndex = 40;
            this.memoTXT.Text = "메모";
            this.memoTXT.Enter += new System.EventHandler(this.memoTXT_Enter);
            this.memoTXT.Leave += new System.EventHandler(this.memoTXT_Leave);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel5.Location = new System.Drawing.Point(51, 448);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(262, 1);
            this.panel5.TabIndex = 41;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("이사만루체 Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(47, 275);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 22);
            this.label3.TabIndex = 45;
            this.label3.Text = "횟수";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("이사만루체 Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(47, 377);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 22);
            this.label4.TabIndex = 46;
            this.label4.Text = "메모";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("이사만루체 Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(47, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 22);
            this.label2.TabIndex = 44;
            this.label2.Text = "item name";
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 600);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.memoTXT);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.numInputTXT);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.itemNameTXT);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.cancelBTN);
            this.Controls.Add(this.confirmBTN);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Form4";
            this.Padding = new System.Windows.Forms.Padding(18, 60, 18, 16);
            this.Resizable = false;
            this.Load += new System.EventHandler(this.Form4_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button confirmBTN;
        private System.Windows.Forms.Button cancelBTN;
        private System.Windows.Forms.TextBox itemNameTXT;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox numInputTXT;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox memoTXT;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
    }
}