﻿namespace BetterToday
{
    partial class Form10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.idTXT = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pwTXT = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cancelBTN = new System.Windows.Forms.Button();
            this.confirmBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // idTXT
            // 
            this.idTXT.BackColor = System.Drawing.SystemColors.Window;
            this.idTXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.idTXT.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idTXT.ForeColor = System.Drawing.Color.Black;
            this.idTXT.Location = new System.Drawing.Point(23, 194);
            this.idTXT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.idTXT.MaxLength = 20;
            this.idTXT.Name = "idTXT";
            this.idTXT.Size = new System.Drawing.Size(350, 23);
            this.idTXT.TabIndex = 46;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel1.Location = new System.Drawing.Point(25, 223);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(350, 1);
            this.panel1.TabIndex = 50;
            // 
            // pwTXT
            // 
            this.pwTXT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pwTXT.BackColor = System.Drawing.SystemColors.Window;
            this.pwTXT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pwTXT.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pwTXT.ForeColor = System.Drawing.Color.Black;
            this.pwTXT.Location = new System.Drawing.Point(25, 197);
            this.pwTXT.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pwTXT.MaxLength = 20;
            this.pwTXT.Name = "pwTXT";
            this.pwTXT.PasswordChar = '*';
            this.pwTXT.Size = new System.Drawing.Size(350, 23);
            this.pwTXT.TabIndex = 49;
            this.pwTXT.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel2.Location = new System.Drawing.Point(23, 221);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(350, 1);
            this.panel2.TabIndex = 48;
            this.panel2.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(21, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(185, 20);
            this.label4.TabIndex = 47;
            this.label4.Text = "변경하실 비밀번호를 입력하세요";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(19, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 20);
            this.label3.TabIndex = 45;
            this.label3.Text = "찾으실 아이디를 입력하세요";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(86, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 43);
            this.label1.TabIndex = 51;
            this.label1.Text = "Better Today";
            // 
            // cancelBTN
            // 
            this.cancelBTN.BackColor = System.Drawing.Color.Gainsboro;
            this.cancelBTN.FlatAppearance.BorderSize = 0;
            this.cancelBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBTN.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.cancelBTN.Location = new System.Drawing.Point(206, 274);
            this.cancelBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelBTN.Name = "cancelBTN";
            this.cancelBTN.Size = new System.Drawing.Size(105, 45);
            this.cancelBTN.TabIndex = 57;
            this.cancelBTN.Text = "취소";
            this.cancelBTN.UseVisualStyleBackColor = false;
            this.cancelBTN.Click += new System.EventHandler(this.cancelBTN_Click);
            // 
            // confirmBTN
            // 
            this.confirmBTN.BackColor = System.Drawing.Color.RoyalBlue;
            this.confirmBTN.FlatAppearance.BorderSize = 0;
            this.confirmBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.confirmBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.confirmBTN.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.confirmBTN.Location = new System.Drawing.Point(81, 274);
            this.confirmBTN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.confirmBTN.Name = "confirmBTN";
            this.confirmBTN.Size = new System.Drawing.Size(105, 45);
            this.confirmBTN.TabIndex = 56;
            this.confirmBTN.Text = "확인";
            this.confirmBTN.UseVisualStyleBackColor = false;
            this.confirmBTN.Click += new System.EventHandler(this.confirmBTN_Click);
            // 
            // Form10
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(396, 357);
            this.Controls.Add(this.cancelBTN);
            this.Controls.Add(this.confirmBTN);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.idTXT);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pwTXT);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "Form10";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox idTXT;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox pwTXT;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancelBTN;
        private System.Windows.Forms.Button confirmBTN;
    }
}