﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetterToday
{
    public class ItemRecordDTO
    {
        public int ItemId { get; set; }
        public DateTime ItemRecordDate { get; set; }
        public int ItemRecordValue { get; set; }
        public string ItemRecordMemo { get; set; }
        public byte ItemRecordAchievement { get; set; }        
    }
}
